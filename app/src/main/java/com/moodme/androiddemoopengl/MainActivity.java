package com.moodme.androiddemoopengl;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.moodme.trackerlib.InvalidLicenseException;
import com.moodme.trackerlib.MDMTrackerManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    public static final String licenseKey = "csv9RchD6jtxNX6ZAe1uGsnCyHZNWySdQScr5CJECXUEg30Nne1qFvl4Sf0KVn3giTdR2PgiY9prxcx+B8zvIprTao8lFk2zVsSXDiTzDLcMXDiSvwQRdUGH0SV9gcK4Wntpvam+umk4Us68zmF/ot/MvqpAPSLnyV85QGvuLSqywwMRf4tcMca3+IEm7QoRs85IcEXo2gK9uQ46yt5WagSUoknW31xeL8/62s5cMC1VyDj0m+ZLdzIZAOp4IK089GzrHISLe82qtVcIHAFtKhqlgkvHoVDIlDwbCicL2nzN3nDXM3sRzlQSQ8ItmrhCd1GNzeD6Ui6nsHTCkzik3A==";

    public static final int CAMERA_PERMISSION_REQUEST_CODE = 3;

    private MySurfaceView glSurfaceView;
    private MyRenderer renderer;
    private MyCameraProvider cameraProvider;
    private MDMTrackerManager tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            init();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                Toast.makeText(this, "Camera permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            init();
        } else {
            Toast.makeText(this, "Camera permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        }
    }

    private void init() {
        createTrackerManager();
        createRenderer();
        createGLSurfaceView();
        createCameraProvider();

        setContentView(glSurfaceView);
    }

    private void createTrackerManager() {
        try {
            tracker = new MDMTrackerManager(this, licenseKey);
        } catch (InvalidLicenseException e) {
            e.printStackTrace();
        }
    }

    private void createRenderer() {
        renderer = new MyRenderer(getApplicationContext());
    }

    private void createGLSurfaceView() {
        glSurfaceView = new MySurfaceView(this, renderer);
    }

    private void createCameraProvider() {
        FPSUpdater updater = new FPSUpdater() {
            @Override
            public void update(double fps) {
                NumberFormat formatter = new DecimalFormat("#0.00");
                MainActivity.this.setTitle("" + formatter.format(fps) + " fps");
            }
        };
        SurfaceTexture surfaceTexture = new SurfaceTexture(renderer.getTextureHandle());
        cameraProvider = new MyCameraProvider(getApplicationContext(), surfaceTexture, renderer, tracker, updater);
        surfaceTexture.setOnFrameAvailableListener(cameraProvider);
    }
}
