package com.moodme.androiddemoopengl;

/**
 * Created by dfomin on 05/08/16.
 */
public interface FPSUpdater {
    public void update(double fps);
}
