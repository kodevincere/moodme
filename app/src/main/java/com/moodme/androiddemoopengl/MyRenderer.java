package com.moodme.androiddemoopengl;

/**
 * Created by dfomin on 30/07/16.
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.util.SparseIntArray;

import com.threed.jpct.Camera;
import com.threed.jpct.FrameBuffer;
import com.threed.jpct.GenericVertexController;
import com.threed.jpct.Light;
import com.threed.jpct.Loader;
import com.threed.jpct.Matrix;
import com.threed.jpct.Mesh;
import com.threed.jpct.Object3D;
import com.threed.jpct.RGBColor;
import com.threed.jpct.SimpleVector;
import com.threed.jpct.Texture;
import com.threed.jpct.TextureManager;
import com.threed.jpct.World;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by dfomin on 30/07/16.
 */
public class MyRenderer implements GLSurfaceView.Renderer {

    private static final String TAG = MyRenderer.class.getName();
    private final int UNIQUE_VERTICES = 93;

    private World world;
    private FrameBuffer frameBuffer;
    private Object3D pumpkin;

    private String pattern = "([0-9]+\\/)+[0-9]+";
    private String fPattern = "([0-9]+\\/){2}[0-9]+";
    private String pPattern = "p ([0-9]+\\/)+[0-9]+";

    private ArrayList<Integer> allVertex = new ArrayList<>(429);
    private ArrayList<Integer> uniqueVertices = new ArrayList<>(UNIQUE_VERTICES);
    private ArrayList<Integer> points = new ArrayList<>(UNIQUE_VERTICES);
    private SparseIntArray pointInUniquePosition = new SparseIntArray(UNIQUE_VERTICES);

    private Mesh mesh;

    float[] face3DVertices;
    float[] face3DModelView;


    //Coordenadas que definen los 4 puntos que encuadran la cámara en el GLSurfaceView
    private float vertexCoords[] = {
            -1.0f, -1.0f, -10.0f,
            1.0f, -1.0f, -10.0f,
            -1.0f,  1.0f, -10.0f,
            1.0f,  1.0f, -10.0f
    };

    // coordinates fixed for 90 degree rotation
    private float textureCoords[] = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
    };

    private FloatBuffer vertexBuffer;
    private FloatBuffer textureBuffer;
    private FloatBuffer landmarksBuffer;
//-1
//0
    private final String vertexShaderCode = "" +
            "attribute vec2 vPosition;\n" +
            "attribute vec2 vTexCoord;\n" +
            "varying vec2 texCoord;\n" +
            "void main() {\n" +
            "  texCoord = vTexCoord;\n" +
            "  gl_Position = vec4(vPosition.x, vPosition.y, 1.0, 1.0);\n" +
            "}";

    private final String fragmentShaderCode = "" +
            "#extension GL_OES_EGL_image_external : require\n" +
            "precision mediump float;\n" +
            "uniform samplerExternalOES sTexture;\n" +
            "varying vec2 texCoord;\n" +
            "void main() {\n" +
            "  gl_FragColor = texture2D(sTexture,texCoord);\n" +
            "}";

    private final String landmarksVertexShaderCode = "" +
            "attribute vec2 vPosition;\n" +
            "void main() {\n" +
            "  gl_PointSize = 25.0;\n" +
            "  gl_Position = vec4(vPosition.x, vPosition.y, 1.0, 1.0);\n" +
            "}";

    private final String landmarksFragmentShaderCode = "" +
            "precision mediump float;\n" +
            "void main() {\n" +
            "  gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n" +
            "}";

    private int cameraProgram;
    private int programLandmarks;
    private int textureHandle;
    private SurfaceTexture surfaceTexture;
    private boolean needUpdateSurfaceTexture = false;
    private boolean landmarksFound = false;
    private Context context;
    private float cameraAspectRatio = 0;
    private float surfaceAspectRatio = 0;
    private boolean updateFace = false;


    public MyRenderer(Context context) {
        this.context = context;
    }

    public int getTextureHandle() {
        return textureHandle;
    }

    public void setAspectRatio(float ratio) {
        cameraAspectRatio = ratio;

        updateAspectRatio();
    }

    private void updateAspectRatio() {
        /*if (cameraAspectRatio == 0 || surfaceAspectRatio == 0) {
            return;
        }

        Log.e(TAG, "" + cameraAspectRatio + " " + surfaceAspectRatio);
        float heightFix = 0;
        float widthFix = 0;
        if (cameraAspectRatio < surfaceAspectRatio) {
            heightFix = surfaceAspectRatio / cameraAspectRatio - 1;
        } else {
            widthFix = cameraAspectRatio / surfaceAspectRatio - 1;
        }

        Log.e(TAG, "" + widthFix + " " + heightFix);

        float[] newTexCoords = textureCoords.clone();
        for (int i = 0; i < 4; ++i) {
            if (newTexCoords[2*i] < 0.5) {
                newTexCoords[2*i] += widthFix;
            } else {
                newTexCoords[2*i] -= widthFix;
            }

            if (newTexCoords[2*i+1] < 0.5) {
                newTexCoords[2*i+1] += heightFix;
            } else {
                newTexCoords[2*i+1] -= heightFix;
            }
        }

        textureBuffer.clear();
        textureBuffer.put(newTexCoords);
        textureBuffer.position(0);*/
    }

    public synchronized void updateTexture(SurfaceTexture surfaceTexture) {
        needUpdateSurfaceTexture = true;
        this.surfaceTexture = surfaceTexture;
    }

    public synchronized void updateLandmarks(float[] landmarks) {
        if (landmarks != null) {
            landmarksBuffer.put(landmarks);
            landmarksBuffer.position(0);
            landmarksFound = true;
        } else {
            landmarksFound = false;
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        world = new World();
        createBuffers();
        configureOpenGL();
        addLights();
        addObjects();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {

        GLES20.glViewport(0, 0, width, height);

        frameBuffer = new FrameBuffer(width, height);

        surfaceAspectRatio = ((float)height / (float)width);
        updateAspectRatio();
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
        frameBuffer.clear(new RGBColor(255,255,255,0));

        synchronized(this) {
            if (needUpdateSurfaceTexture) {
                surfaceTexture.updateTexImage();
                needUpdateSurfaceTexture = false;
            }
        }

         //Draw camera frame
        GLES20.glUseProgram(cameraProgram);

        int positionHandle = GLES20.glGetAttribLocation(cameraProgram, "vPosition");
        int texCoordHandle = GLES20.glGetAttribLocation(cameraProgram, "vTexCoord");

        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false, 3 * 4, vertexBuffer);

        GLES20.glEnableVertexAttribArray(texCoordHandle);
        GLES20.glVertexAttribPointer(texCoordHandle, 2, GLES20.GL_FLOAT, false, 2 * 4, textureBuffer);
//
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, vertexCoords.length / 3);
//
        GLES20.glDisableVertexAttribArray(positionHandle);
        GLES20.glDisableVertexAttribArray(texCoordHandle);

        // Draw face landmarks
//        if (landmarksFound) {
//            GLES20.glUseProgram(programLandmarks);
//
////            int landmarkPositionHandle1 = GLES20.glGetAttribLocation(cameraProgram, "vPosition");
//            int landmarkPositionHandle = GLES20.glGetAttribLocation(programLandmarks, "vPosition");
//
//            GLES20.glVertexAttribPointer(landmarkPositionHandle, 2, GLES20.GL_FLOAT, false, 2 * 4, landmarksBuffer);
//            GLES20.glEnableVertexAttribArray(landmarkPositionHandle);
//
//            GLES20.glDrawArrays(GLES20.GL_POINTS, 0, 66);
//
//            GLES20.glDisableVertexAttribArray(landmarkPositionHandle);
//        }

        if(updateFace)
            drawFace();

        world.renderScene(frameBuffer);
        world.draw(frameBuffer);
        frameBuffer.display();

    }

    private int loadShader(int type, String shaderCode) {
        int shader;

        shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        // Check shader compile status
        int compileStatus[] = {GLES20.GL_FALSE};
        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if(compileStatus[0] == GLES20.GL_FALSE) {
            int logSize[] = {0};
            GLES20.glGetShaderiv(shader, GLES20.GL_INFO_LOG_LENGTH, logSize, 0);
            if(logSize[0] > 0) {
                String errorLog = GLES20.glGetShaderInfoLog(shader);
                Log.d(TAG, errorLog);
            }
        }

        return shader;
    }

    private void configureOpenGL() {
        cameraProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(cameraProgram, loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode));
        GLES20.glAttachShader(cameraProgram, loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode));
        GLES20.glLinkProgram(cameraProgram);

//        GLES20.glUseProgram(cameraProgram);
//        int texture;
//        texture = GLES20.glGetUniformLocation(cameraProgram, "texture");
//        GLES20.glUniform1i(texture, 0);
//
//        Log.e("PUTA", "Texture: " + texture);

        programLandmarks = GLES20.glCreateProgram();
        GLES20.glAttachShader(programLandmarks, loadShader(GLES20.GL_VERTEX_SHADER, landmarksVertexShaderCode));
        GLES20.glAttachShader(programLandmarks, loadShader(GLES20.GL_FRAGMENT_SHADER, landmarksFragmentShaderCode));
        GLES20.glLinkProgram(programLandmarks);

        int[] textureHandles = new int[1];
        GLES20.glGenTextures(1, textureHandles, 0);
        textureHandle = textureHandles[0];
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        // Use GL_TEXTURE_EXTERNAL_OES instead of GL_TEXTURE0 for video stream comes from SurfaceTexture
        // http://developer.android.com/reference/android/graphics/SurfaceTexture.html
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureHandle);

        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
    }

    private void createBuffers() {
        ByteBuffer byteBuffer;

        byteBuffer = ByteBuffer.allocateDirect(vertexCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertexCoords);
        vertexBuffer.position(0);

        byteBuffer = ByteBuffer.allocateDirect(textureCoords.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        textureBuffer = byteBuffer.asFloatBuffer();
        textureBuffer.put(textureCoords);
        textureBuffer.position(0);

        byteBuffer = ByteBuffer.allocateDirect(66 * 2 * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        landmarksBuffer = byteBuffer.asFloatBuffer();
    }

    Camera camera;
    Matrix modelViewMatrix = new Matrix();
    private void addObjects() {
        Object3D face = createFaceModel();

        camera = world.getCamera();

        world.addObject(face);

        camera.setPosition(0, 0, -150);
        camera.lookAt(face.getTransformedCenter());

    }


    private Object3D createFaceModel() {
        Resources res = context.getResources();

        TextureManager textureManager = TextureManager.getInstance();
        Texture azog = new Texture(res.openRawResource(R.raw.model0));
        textureManager.addTexture("model0.png", azog);

        InputStream pumpkinIS = res.openRawResource(R.raw.model);

        String objModel = openFileInputStream(pumpkinIS, false);
        processObjFileContent(objModel);

        pumpkinIS = res.openRawResource(R.raw.model);
        InputStream pumpkinMtIS = res.openRawResource(R.raw.model_mtl);
        Object3D[] models = Loader.loadOBJ(pumpkinIS, pumpkinMtIS, 1f);

        pumpkin = models[0];
        pumpkin.setCenter(SimpleVector.ORIGIN);
        pumpkin.translate(new SimpleVector(0.0f, 0.0f, 0.0f));

        mesh = pumpkin.getMesh();
        mesh.setVertexController(new Controller(pointInUniquePosition, this.points), false);

        pumpkin.build();
        pumpkin.compile();

        return pumpkin;
    }

    private void processObjFileContent(String objFileContent) {
        // Create a Pattern object
        Pattern innerFPattern = Pattern.compile(fPattern);
        Pattern innerPPattern = Pattern.compile(pPattern);
        // Now create matcher object.
        Matcher fMatcher = innerFPattern.matcher(objFileContent);
        Matcher pMatcher = innerPPattern.matcher(objFileContent);

        while(fMatcher.find())
            processVertex(fMatcher.group(0));

        while(pMatcher.find())
            processPoint(pMatcher.group(0));

    }

    private void processVertex(String group) {
        String[] numbers = group.split("/");
        Integer i = Integer.parseInt(numbers[0]);
        if(!uniqueVertices.contains(i))
            uniqueVertices.add(i);
        allVertex.add(i);
    }

    private void processPoint(String group) {
        String[] numbers = group.split("/");
        Integer actualPoint = Integer.parseInt(numbers[0].substring(2));
        points.add(actualPoint);

        int uniqueVertexIndex = uniqueVertices.indexOf(new Integer(actualPoint));
        pointInUniquePosition.put(actualPoint, uniqueVertexIndex);

    }

    private String openFileInputStream(InputStream inputStream, boolean closeAtFinish) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String everything = "";
        try {
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = reader.readLine();
            }
            everything = sb.toString();

            if(closeAtFinish)
                reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return everything;
    }
    private void addLights(){
        Light light = new Light(world);
        light.setIntensity(255, 255, 255);
        light.setPosition(SimpleVector.ORIGIN);
    }

    public void update3DModelView(float[] trackerModelView) {
        face3DModelView = trackerModelView;
    }

    public void update3DVertices(float[] vertices3D) {
        face3DVertices = vertices3D;
        updateFace = true;
    }

    private void drawFace(){

        mesh.applyVertexController();
//        modelViewMatrix.fillDump(face3DModelView);
        modelViewMatrix.setDump(face3DModelView);
//        modelViewMatrix.rotateX((float) (Math.PI));
//        modelViewMatrix.transformToGL();
        printMatrix(face3DModelView);
        camera.setBack(modelViewMatrix);
        pumpkin.touch();
        updateFace = false;
    }

    class Controller extends GenericVertexController {

        private ArrayList<Integer> points;
        private SparseIntArray pointInUniquePosition;
        private int length = 66;

        public Controller(SparseIntArray pointInUniquePosition, ArrayList<Integer> points){
            this.points = points;
            this.pointInUniquePosition = pointInUniquePosition;
        }

        @Override
        public void apply() {

    /* actualPointPosition recorre en orden cada uno de los p XX/XX.
     Estos p XX/XX son el orden en el que MoodMe devuelve los vértices 3D.
     Cada que se crea un vértice para dibujar en 3D, se agrega a la lista de vértices únicos.
     Estos vértices únicos son los que componen el objeto a dibujar.
     uniqueVertexIndex Es la posición en la que se encuentra el vértice alojado en actualPointPosition
     dentro de al lista de vértices únicos.
     Teniendo esos datos, es posible definir que la posición uniqueVertexIndex dentro de la malla destino
     debe ser modificada a la posición i que devuelve MoodMe*/

            SimpleVector[] d = getDestinationMesh();

            for (int i = 0; i < length; i++) {
                int actualPoint = points.get(i);
                int uniqueVertexIndex = pointInUniquePosition.get(actualPoint);
                d[uniqueVertexIndex] = getVectorInPosition(i);
            }
        }
    }




    private SimpleVector getVectorInPosition(int position){
        float x = face3DVertices[3*position];
        float y = face3DVertices[3*position + 1];
        float z = face3DVertices[3*position + 2];
        return new SimpleVector(x, y, z);
    }

    public static void printMatrix(float[] matrix){

        int rows = (int) Math.sqrt(matrix.length);

        StringBuilder row = new StringBuilder(100);

        for (int i = 0; i < rows; i++) {
            row.append(String.format("%.2f %.2f %.2f %.2f\n", matrix[4 * i], matrix[4 * i + 1], matrix[4 * i + 2], matrix[4 * i + 3]));
        }

        Log.e("PUTA", String.format("La matriz es:\n %s", row.toString()));

    }

}
