package com.moodme.androiddemoopengl;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.media.Image;
import android.media.ImageReader;
import android.view.Surface;

import com.moodme.trackerlib.MDMTrackerManager;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by dfomin on 30/07/16.
 */
public class MyCameraProvider implements SurfaceTexture.OnFrameAvailableListener, ImageReader.OnImageAvailableListener {

    private static final String TAG = MyCameraProvider.class.getName();

    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;

    private CaptureRequest.Builder previewRequestBuilder;
    private SurfaceTexture surfaceTexture;
    private MyRenderer renderer;
    private ImageReader imageReader;
    private MDMTrackerManager tracker;
    private FPSUpdater updater;

    private long lastFrameTime = 0;

    public MyCameraProvider(Context context, SurfaceTexture surfaceTexture, MyRenderer renderer, MDMTrackerManager tracker, FPSUpdater updater) {
        this.surfaceTexture = surfaceTexture;
        this.renderer = renderer;
        renderer.setAspectRatio((float)WIDTH / (float)HEIGHT);
        this.tracker = tracker;
        this.updater = updater;
        openCamera(context);
    }

    private void openCamera(Context context) {
        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT) {
                    manager.openCamera(cameraId, cameraDeviceStateCallback, null);
                    break;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
    }

    private final CameraDevice.StateCallback cameraDeviceStateCallback =
            new CameraDevice.StateCallback() {
                @Override
                public void onOpened(CameraDevice camera) {
                    surfaceTexture.setDefaultBufferSize(WIDTH, HEIGHT);
                    Surface surface = new Surface(surfaceTexture);

                    try {
                        previewRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }

                    previewRequestBuilder.addTarget(surface);

                    imageReader = ImageReader.newInstance(WIDTH, HEIGHT, ImageFormat.YUV_420_888, 2);
                    imageReader.setOnImageAvailableListener(MyCameraProvider.this, null);

                    previewRequestBuilder.addTarget(imageReader.getSurface());

                    try {
                        camera.createCaptureSession(Arrays.asList(surface, imageReader.getSurface()), cameraCaptureSessionStateCallback, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onDisconnected(CameraDevice camera) {
                }

                @Override
                public void onError(CameraDevice camera, int error) {
                }
            };

    private final CameraCaptureSession.StateCallback cameraCaptureSessionStateCallback =
            new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(CameraCaptureSession session) {
                    CaptureRequest previewRequest = previewRequestBuilder.build();
                    try {
                        session.setRepeatingRequest(previewRequest, null, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            };

    @Override
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {

        renderer.updateTexture(surfaceTexture);

        if (tracker.isFaceTracked()) {
            // translate to opengl coordinates
            float[] landmarks = new float[66*2];
            float[] trackerLandMarks = tracker.getLandmarks();

            float[] tracker3DVertices = tracker.getVertices();
            float[] trackerModelView = tracker.getModelView();

            for (int i = 0; i < 66; ++i) {
                landmarks[2*i] = 1.0f - trackerLandMarks[2*i] / (HEIGHT / 2);
                landmarks[2*i+1] = 1.0f - trackerLandMarks[2*i+1] / (WIDTH / 2);

            }

            renderer.updateLandmarks(landmarks);
            renderer.update3DModelView(trackerModelView);
            renderer.update3DVertices(tracker3DVertices);

        } else {
            renderer.updateLandmarks(null);
        }
    }

    @Override
    public void onImageAvailable(ImageReader reader) {

        long startTime = System.currentTimeMillis();
        Image image =  imageReader.acquireNextImage();
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        byte[] rotated = rotate(bytes, WIDTH, HEIGHT);
        tracker.processImageBuffer(rotated, HEIGHT, WIDTH, HEIGHT);

        image.close();
        long currentTime = System.currentTimeMillis();
        double fps = 1000.0 / (currentTime - lastFrameTime);
        double fps1 = 1000.0 / (currentTime - startTime);

        updater.update(fps);

        //Log.e(TAG, "result: " + tracker.isFaceTracked() + " fps: " + fps + " fps1: " + fps1);
        lastFrameTime = currentTime;
    }

    private byte[] rotate(byte[] original, int width, int height) {
        byte[] rotated = new byte[original.length];

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                rotated[height * (width - x - 1) + y] = original[y * width + x];
            }
        }

        return rotated;
    }
}
