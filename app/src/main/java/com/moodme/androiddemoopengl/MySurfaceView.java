package com.moodme.androiddemoopengl;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by dfomin on 30/07/16.
 */
public class MySurfaceView extends GLSurfaceView {
    public MySurfaceView(Context context, GLSurfaceView.Renderer renderer) {
        super(context);

        setEGLContextClientVersion(2);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);
//        getHolder().setFormat(PixelFormat.TRANSPARENT);
        setRenderer(renderer);
    }
}
